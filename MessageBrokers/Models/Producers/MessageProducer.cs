﻿using System;
namespace MessageBrokers.Models.Producers
{
    public abstract class MessageProducer : IMessageProducer
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public  MessageProducer(string host, string username, string password)
        {
            HostName = host;
            UserName = username;
            Password = password;
        }

        public abstract void Send<T>(T message);
    }
}
