﻿using System;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace MessageBrokers.Models.Producers
{
    public class RabbitMQPublisher : MessageProducer
    {
        private ConnectionFactory _factory;
        public string Topic { get; set; } 

        public RabbitMQPublisher(string host, string username, string password, string topic)
            : base(host, username, password)
        {
            Topic = topic;
            _factory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
            };

            using (var connection = _factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: Topic, type: "fanout");
                }
            }
        }

        public override void Send<T>(T message)
        {
            using (var connection = _factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var toSend = JsonConvert.SerializeObject(message);
                    var body = Encoding.UTF8.GetBytes(toSend);

                    channel.BasicPublish(exchange: Topic,
                                         routingKey: "",
                                         basicProperties: null,
                                         body: body);
                }
            }
        }
    }
}
