﻿using Amazon;
using Amazon.SimpleNotificationService.Model;
using Amazon.SimpleNotificationService;
using Amazon.Runtime;
using Newtonsoft.Json;

namespace MessageBrokers.Models.Producers
{
    public class SNSProducer : MessageProducer
    {
        private readonly BasicAWSCredentials _credentials;
        private RegionEndpoint Endpoint;

        public SNSProducer(string topicArn, string username, string password, RegionEndpoint endpoint)
            : base(topicArn, username, password)
        {
            _credentials = new BasicAWSCredentials(UserName, Password);
            Endpoint = endpoint;
        }

        public override void Send<T>(T message)
        {
            var snsClient = new AmazonSimpleNotificationServiceClient(_credentials, Endpoint);

            var toSend = JsonConvert.SerializeObject(message);

            snsClient.PublishAsync(new PublishRequest
            {
                Message = toSend,
                TopicArn = HostName
            }).Wait();


        }
    }
}
