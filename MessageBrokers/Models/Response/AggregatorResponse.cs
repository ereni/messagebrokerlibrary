﻿using System.Collections.Generic;
using System.Linq;

namespace MessageBrokers.Models.Response
{
    public class AggregatorResponse
    {
        public IEnumerable<WorkerResponse> Responses { get; private set; }

        public AggregatorResponse()
        {
        }

        public void AddResponse(WorkerResponse response)
        {
            if (!Responses.Contains(response))
                Responses = Responses.Concat(new[] { response });
        }
    }
}
