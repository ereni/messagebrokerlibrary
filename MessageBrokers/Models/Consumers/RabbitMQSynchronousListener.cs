﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Amazon.SimpleNotificationService.Model;

namespace MessageBrokers.Models.Consumers
{

    public class RabbitMQSynchronousListener : RabbitMQListener
    {
        public RabbitMQSynchronousListener(string host, string username, string password, string topic) 
            : base(host, username, password, topic)
        {
        }

        protected override string GetMessage(IModel channel)
        {
            var work = String.Empty;
            while (String.IsNullOrEmpty(work))
            {
                var body = channel.BasicGet(queue: QueueName, autoAck: true);
                if (body != null)
                {
                    work = Encoding.UTF8.GetString(body.Body);
                }
                Thread.Sleep(1);
            }

            return work;
        }
    }
}
