﻿namespace MessageBrokers.Models.Consumers
{
    public abstract class WorkListener<T> : IWorkListener<T>
    {
        public abstract T ListenForWork();

        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        protected WorkListener(string host, string username, string password)
        {
            HostName = host;
            UserName = username;
            Password = password;
        }
    }
}
